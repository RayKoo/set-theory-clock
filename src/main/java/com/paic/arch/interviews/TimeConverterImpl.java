package com.paic.arch.interviews;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeConverterImpl implements TimeConverter {
	
	private static final String YELLOW="Y";
	private static final String RED="R";
	private static final String EMPTY="O";
	private String[] fiveHou={EMPTY, EMPTY, EMPTY, EMPTY};
	private String[] oneHou={EMPTY, EMPTY, EMPTY, EMPTY};
	private String[] fiveMin={EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY, EMPTY,EMPTY, EMPTY, EMPTY};
	private String[] oneMin={EMPTY, EMPTY, EMPTY, EMPTY};
	private String[] sec={EMPTY};
	private StringBuffer result;
	
	private TimeConverterImpl(){
		
	}
	
	@Override
	public String convertTime(String aTime) {
		Pattern pattern = Pattern.compile("\\d:\\d:\\d"); 
        Matcher matcher = pattern.matcher(aTime); 
        if(matcher.find()){
        	String[] timeArr=aTime.split(":");
        	this.convertSec(Integer.parseInt(timeArr[3]));
        	this.convertHou(Integer.parseInt(timeArr[1]));
        	this.convertMin(Integer.parseInt(timeArr[2]));
        	return result.toString();
        }
		return null;
	}
	
	/**
	 * 转换小时：
	 * 1.以5取余，得到小时第二排要亮灯的个数
	 * 2.小时减去1中的结果再除以5，得到小时第一排要亮灯的个数
	 * @param hou
	 */
	private void convertHou(int hou){
		if(hou<24){
			int countOneHou=hou%5;
			int countFiveHou=(hou-countOneHou)/5;
			for(int i=0;i<countFiveHou;i++){
				fiveHou[i]=RED;
			}
			for(int j=0;j<countOneHou;j++){
				oneHou[j]=RED;
			}
		}
		this.converStr(fiveHou);
		this.converStr(oneHou);
	}
	
	/**
	 * 转换分钟
	 * 1.以5取余，得到分钟第二排要亮灯的个数
	 * 2.分钟减去1中的结果再除以5，得到分钟第一排要亮灯的个数
	 * @param min
	 */
	private void convertMin(int min){
		if(min<60){
			int countOneMin=min%5;
			int countFiveMin=(min-countOneMin)/5;
			for(int t=0;t<countFiveMin;t++){
				if(t==2 || t==5 || t==8){
					fiveMin[t]=RED;
				}else{
					fiveMin[t]=YELLOW;
				}
				
			}
			for(int k=0;k<countOneMin;k++){
				oneMin[k]=YELLOW;
			}
		}
		this.converStr(fiveMin);
		this.converStr(oneMin);
	}
	
	/**
	 * 转换秒
	 * 偶数亮灯，奇数不亮
	 * @param second
	 */
	private void convertSec(int second){
		sec[0]=second%2==1?EMPTY:YELLOW;
		this.converStr(sec);
	}
	
	private void converStr(String[] args){
		for(int i = 0; i < args.length; i++){
		 result. append(args[i]);
		}
		result.append("\r\n");
	}
	
	

}
